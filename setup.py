"""Describe distribution to setuptools."""
# Import built-in modules
import os
from pathlib import Path

# Import third-party modules
from setuptools import find_packages, setup


def list_data():
	"""Get all test data files from the test/data directory.
	Returns:
			list: Absolute paths of all test data files.
	"""
	fpn = Path(__file__).parent.resolve()
	files = [str(x) for x in fpn.glob("**")]
	return files


setup(
	name="hj_tools",
	author="Alex Joss",
	author_email="joss13@gmail.com",
	url="git@gitlab.com:m5995/hj_tools.git",
	package_dir={"": "."},
	packages=find_packages(),
	description="Houdini Joss Tools",
	entry_points={
		"console_scripts": [
			"Clip = hj_tools.__init__:start_clip",
			"Envexp = hj_tools.__init__:start_envexp",
			"Renamer = hj_tools.main:renamer",
		],
		"hj_tools_preflight.checks": [],
		"preflight.action": [],
	},
	package_data={
		# Additional package data
		"hj_tools": list_data()
	},
	classifiers=[
		"Programming Language :: Python",
		"Programming Language :: Python :: 3",
	],
	use_scm_version=True,
	setup_requires=["setuptools_scm"],
	install_requires=[
		"PySide6",
		"Qt.py",
	],
)
