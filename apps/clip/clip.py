#!/usr/bin/python
# -*- coding: utf-8 -*-
# System modules
import json
import logging
import os
import sys
from pathlib import Path
from typing import Union

import hj_tools.vendor.appdirs as appdirs

# Logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"')
LOG = logging.getLogger(__name__)
# LOG.setLevel(logging.INFO)
# Bootstrap Qt
from hj_tools.libs.initlib import initlib

Host = initlib.get_host()
Qt = initlib.get_qt_mod()
from Qt import QtCompat, QtCore, QtGui, QtWidgets

LOG.debug(f"//----------------------\nHost: {Host}\nQt: {Qt}")
try:
	import hou
	import toolutils
except ImportError:
	LOG.debug("Houdini API is not loaded.")
	pass
# Ui
from .ui.clip_ui import Ui_Clip
from .ui.clip_widget_ui import Ui_ClipboardWidget

__all__ = ["Clip", "ClipWidget"]


class TreeView(QtWidgets.QTreeView):
	"""Add delete button to every QTableWidget entry."""

	# Disable UnicodeStringViolation: we need unicode symbol for Python2.
	changed = QtCore.Signal()

	def __init__(self, parent=None):
		"""Initialize custom widget data.
	Args:
		parent (QObject): Qt parent widget.
		"""
		super(TreeView, self).__init__(parent=parent)

	def paintEvent(self, event):
		"""Display friendly message if list is empty.
	Args:
		event (QPaintEvent): Qt event.
		"""
		super(TreeView, self).paintEvent(event)
		if not self.model():
			painter = QtGui.QPainter(self.viewport())
			color = self.palette().text().color()
			color.setAlpha(32)
			painter.setPen(color)
			painter.setFont(QtGui.QFont("Arial", 22))
			painter.drawText(self.geometry(), QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop, 'Path does not exist,\nuse "Add folder" button\nor browse different path')


class ClipWidget(QtWidgets.QWidget, Ui_ClipboardWidget):
	ORG = "aJoss"
	TITLE = "ClipWidget"
	a_clip_dir = "cg_clipboard/hou"
	a_json = ".json"
	a_default = "default"
	a_default_root = "/obj"
	sig_presets_changed = QtCore.Signal()

	def __init__(self, parent=None, name="Clipboard1", path=None):
		"""
	Args:
		parent: parent widget
		name: object name - used in saveUI as a namespace
		"""
		LOG.debug("start")
		LOG.debug("parent is: %s", parent)
		parent = parent or self.getParent()
		super().__init__(parent)
		#
		self._presets = {}  # presets name:Path dict
		self._tv_model = None
		self._initialized = False
		self._in_name = name
		self._in_path = path
		#
		self.setObjectName(self._in_name)
		QtCore.QCoreApplication.setOrganizationDomain(self.TITLE)
		QtCore.QCoreApplication.setOrganizationName(self.TITLE)
		QtCore.QCoreApplication.setApplicationName(self.TITLE)
		self.settings = QtCore.QSettings()
		#
		# self.initClipboard()
		self.initUI()
		self.initSignals()
		self.initData()
		LOG.debug("init done")

	@staticmethod
	def getParent():
		"""Returns parent window"""
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py in ("hython", "houdini", "houdinifx"):
			return hou.qt.mainWindow()

	def initUI(self):
		LOG.debug("init UI")
		self.setupUi(self)
		# Little hack with custom TreeView
		self.tv0 = TreeView(self)
		self.tv0.setObjectName("tv0")
		self.gl_tv5.addWidget(self.tv0, 0, 0, 1, 1)
		self.loadUI()
		self.setWindowTitle(self._in_name)

	def initSignals(self):
		LOG.debug("init Signals")
		self.sig_presets_changed.connect(self._on_presets_changed)
		pass

	def initData(self):
		LOG.debug("init Data")
		if not self._in_path:
			self._in_path = str(self.get_presets_dir())
		self.le_storage_path.setText(str(self._in_path))
		# self.refreshAll()
		pass

	def refreshAll(self) -> None:
		LOG.debug("refresh all")
		self._initialized = False
		# init clipboard folders
		pth = Path(self._in_path)
		# Use default dir if folder not found
		if not pth.exists():
			LOG.debug(f"path {pth} does not exist, skip")
			self.tv0.setModel(None)
			return
		LOG.debug(f"set presets path {pth}")
		self._tv_model = QtWidgets.QFileSystemModel(self)
		self._tv_model.setRootPath(str(pth))
		# init folders model
		self.tv0.setModel(self._tv_model)
		self.tv0.setRootIndex(self._tv_model.setRootPath(str(pth)))
		self.tv0.setColumnWidth(0, 200)
		self._initialized = True

	def stat_msg(self, msg=None):
		"""Emit app message (goes to statusbar and log)."""
		if msg is None:
			return
		# self.statusbar.showMessage(msg)
		LOG.debug(msg)

	########## Small library
	@staticmethod
	def text_dlg(parent=None, title="", label="", text=""):
		return QtWidgets.QInputDialog.getText(parent, title, label, QtWidgets.QLineEdit.Normal, text)

	@staticmethod
	def yesno_dlg(parent=None, title="Continue?", msg="Continue?"):
		res = QtWidgets.QMessageBox.question(parent, msg, title, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
		if res == QtWidgets.QMessageBox.Yes:
			return True
		return False

	@staticmethod
	def browse_dir_dlg(parent=None, title="Select a folder", start=None):
		if not start:
			start = Path(__file__).parent
		res = QtWidgets.QFileDialog.getExistingDirectory(parent, caption=title, dir=str(start))
		pth = Path(res) if res else None
		return pth

	@staticmethod
	def rm_tree(pth: Path):
		"""Unlink Path recursively
	Args:
		pth:
	Returns:
		"""
		for child in pth.glob("*"):
			if child.is_file():
				child.unlink()
			else:
				ClipWidget.rm_tree(child)
		pth.rmdir()

	@staticmethod
	def get_presets_dir() -> Path:
		"""Get presets directory.
	Returns:
		Path object
		"""
		user_data_dir = Path(appdirs.user_data_dir())
		return user_data_dir / ClipWidget.a_clip_dir

	def get_tree_selection(self):
		"""Get currently selected items Paths()
	Returns:
		List of Path() objects
		"""
		qfs_model = self.tv0.model()
		sel_model = self.tv0.selectionModel()
		# selectgedIndexes() gives all columns at once, we need just the first one
		indices = [x for x in sel_model.selectedIndexes() if x.column() == 0]
		dt = [Path(qfs_model.filePath(x)) for x in indices]
		return dt

	def get_tree_cur_dir(self):
		"""Get current folder in a tree view. Presets_dir if none selected.
	Returns:
		Path()
		"""
		items = self.get_tree_selection()
		if items:
			root_dir = items[-1]
			if root_dir.is_file():
				root_dir = root_dir.parent
		else:
			# TODO: remove that function and add safe workaround
			root_dir = self.get_presets_dir()
		return root_dir

	def get_save_filename(self, name):
		"""Generate filepath from given preset name
	Args:
		name: 'test'
	Returns:
		Path('c:/temp/.../test.mat') object
		"""
		# get root dir to save to
		root_dir = self.get_tree_cur_dir()
		# Generate name:
		ext = self.a_json if self.get_save_json_option() else f".{self.get_current_network()}"
		if not name.endswith(ext):
			name += ext
		fpn = root_dir / name
		return fpn

	def get_save_json_option(self):
		return self.cb_save_json_option.isChecked() is True

	def get_save_hda_option(self):
		return self.cb_save_hda_option.isChecked() is True

	def get_confirm_removals_option(self):
		"""placeholder function"""
		# return self.cb_confirm_removals.isChecked() is True
		return True

	def get_confirm_overwrites_option(self):
		"""placeholder function"""
		# return self.cb_confirm_overwrites.isChecked() is True
		return True

	@staticmethod
	def _tokenize2(in_str, keys=" "):
		"""
	Splits string by multiple delimiters:
		print(tokenize('|ns_root:pSphere1_tr_tr_tr|ns_a:nb_aa:pSphere1_tr_tr1|ns_b:pSphere1_tr1.vtx[17]', '|[:.]'))
	Returns parts AND splitters, so they can be zipped again
		"""
		rlist = []
		seplist = []
		zipped = []
		tt = ""
		#
		for i, letter in enumerate(in_str):
			if not letter in keys:
				tt += letter
				continue
			else:
				if tt:
					rlist.append(tt)
					seplist.append(letter)
					zipped.append(tt)
					zipped.append(letter)
					tt = ""
				continue
		if tt:
			rlist.append(tt)
			zipped.append(tt)
		return rlist, seplist, zipped

	# Hou API:
	@staticmethod
	def ls(nodes=None, sl=False):
		"""
		List all/selected nodes in scene / from given nodes.
		:param nodes: if not empty, use nodes as multiple roots for subselections
		:param sl: bool, selected nodes only
		"""
		if sl:
			res = hou.selectedNodes()
		else:
			nodes = nodes or [hou.node("/")]
			lists = [x.allSubChildren() for x in nodes if isinstance(x, hou.Node)]
			res = [x for lst in lists for x in lst]
		return res

	@staticmethod
	def select(nodes=None):
		if not nodes:
			return
		nodes[0].setSelected(True, True)
		for node in nodes:
			node.setSelected(True, False)

	@staticmethod
	def get_pane_type_from_node(node):
		path_parts = [x for x in node.path().split("/") if x]
		res = path_parts[0] if path_parts else None
		return res

	@staticmethod
	def get_current_editor():
		"""Get current editor's Node()
		Returns:
		"""
		return toolutils.networkEditor()

	@staticmethod
	def get_current_path():
		"""Get current editor's path
		Returns:
		"""
		return ClipWidget.get_current_editor().pwd().path()

	@staticmethod
	def get_current_network():
		"""Get current network name
		Returns:
		"""
		return list(filter(None, ClipWidget.get_current_path().split("/")))[0]

	@staticmethod
	def save_to_file(fullpath: Union[Path, None], nodes: [list] = None, save_hda: bool = False) -> Union[Path, None]:
		"""Save selected nodes into preset file.
		Returns: None or Path object
		"""
		if not fullpath:
			LOG.debug(f"bad name: {fullpath}, skipping")
			return None
		if fullpath.is_dir():
			LOG.debug(f"path is a directory: {fullpath}, skipping")
			return None
		if not nodes:
			LOG.debug(f"no nodes selected")
			return None
		root = nodes[0].parent()
		root.saveItemsToFile(nodes, str(fullpath), save_hda_fallbacks=save_hda)
		return fullpath

	@staticmethod
	def load_from_file(fullpath: Union[Path, None] = None) -> Union[list, None]:
		"""Load selected preset and execute the code.
	Args:
		fullpath: name of the preset (not filename!)
	Returns:
		List of nodes
		"""
		if not fullpath:
			LOG.debug(f"bad name: {fullpath}, skipping")
			return None
		if fullpath.is_dir():
			LOG.debug(f"path is a directory: {fullpath}, skipping")
			return None
		net = f"/{fullpath.suffix[1:]}"
		LOG.debug(f"loading {fullpath} into {net}..")
		root = hou.node(net)
		if not root:
			LOG.debug(f'unknown network type "{net}", loading into "{ClipWidget.a_default_root}"')
			root = ClipWidget.a_default_root
		root.loadItemsFromFile(str(fullpath))

	@staticmethod
	def save_to_json(fullpath: Union[Path, None], nodes: [list] = None) -> Union[Path, None]:
		"""Save selected nodes into preset file.
		Returns: None or Path object
		"""
		if not fullpath:
			LOG.debug(f"bad name: {fullpath}, skipping")
			return None
		if fullpath.is_dir():
			LOG.debug(f"path is a directory: {fullpath}, skipping")
			return None
		# nodes = hou.selectedNodes()
		if not nodes:
			LOG.debug(f"no nodes given")
			return None
		codes = [x.asCode(recurse=True) for x in nodes]
		codes_json = json.dumps(codes, indent=4)
		fullpath.write_text(codes_json)
		return fullpath

	@staticmethod
	def load_from_json(fullpath: Union[Path, None] = None) -> Union[list, None]:
		"""Load selected preset and execute the code.
	Args:
		fullpath: name of the preset (not filename!)
	Returns:
		List of nodes
		"""
		if not fullpath:
			LOG.debug(f"bad name: {fullpath}, skipping")
			return None
		if fullpath.is_dir():
			LOG.debug(f"path is a directory: {fullpath}, skipping")
			return None
		code_json = fullpath.read_text()
		LOG.debug(f"{fullpath} has been read")
		codes = json.loads(code_json)
		for i, x in enumerate(codes):
			LOG.debug(f"executing node {i}")
			exec(x)
		nodes = hou.selectedNodes()
		return nodes

	########## CLICK HANDLERS
	@QtCore.Slot()
	def _on_presets_changed(self):
		self.refreshAll()

	@QtCore.Slot()
	def on_b_load_clicked(self):
		sel = self.get_tree_selection()
		if not sel:
			LOG.debug(f"nothing selected")
			return
		fpn = sel[0]
		if fpn.name.endswith(self.a_json):
			self.load_from_json(fpn)
		else:
			self.load_from_file(fpn)

	@QtCore.Slot()
	def on_b_saveas_clicked(self):
		# get nodes
		nodes = hou.selectedNodes()
		if not nodes:
			return
		name = nodes[0].name()
		# Input preset name
		name, dlg_res = self.text_dlg(self, "New preset name", "new name", name)
		if not dlg_res:
			LOG.debug("user cancelled")
			return
		# Generate name:
		fpn = self.get_save_filename(name)
		# Check if fpn exists
		if fpn.exists() and self.cb_confirm_overwrites():
			over_res = self.yesno_dlg(self, "File exists, overwrite?")
			if not over_res:
				LOG.debug("user cancelled")
				return
			else:
				LOG.debug(f"deleting old file: {fpn}")
				fpn.unlink()
		# save
		LOG.debug(f"saving {fpn}")
		if self.get_save_json_option():
			self.save_to_json(fullpath=fpn, nodes=nodes)
		else:
			self.save_to_file(fullpath=fpn, nodes=nodes, save_hda=self.get_save_hda_option())
		self.sig_presets_changed.emit()

	@QtCore.Slot()
	def on_b_remove_clicked(self) -> bool:
		items = self.get_tree_selection()
		if not items:
			LOG.debug(f"nothing selected, skipping")
			return False
		if self.get_confirm_removals_option():
			remove_res = self.yesno_dlg(self, "Remove, are you sure?")
			if not remove_res:
				LOG.debug(f"user cancelled")
				return False
		for fname in items:
			if fname.is_dir():
				LOG.debug(f"removing subtree: {fname}")
				shutil.rmtree(fname)
			else:
				LOG.debug(f"removing {fname}")
				fname.unlink()
		self.sig_presets_changed.emit()
		return True

	@QtCore.Slot()
	def on_b_addfolder_clicked(self) -> bool:
		root_dir = self.get_tree_cur_dir()
		name, dlg_res = self.text_dlg(self, "New directory name", "new name", self.a_default)
		if not dlg_res:
			LOG.debug("user cancelled")
			return False
		new_path = root_dir / name
		if new_path.exists():
			LOG.debug(f"already exists: {new_path}")
			return False
		LOG.debug(f"creating directory: {new_path}")
		new_path.mkdir(parents=True, exist_ok=True)
		self.sig_presets_changed.emit()
		return True

	@QtCore.Slot()
	def on_b_browse_path_clicked(self) -> bool:
		old_path = self.le_storage_path.text()
		new_path = self.browse_dir_dlg(parent=self, start=old_path)
		if new_path:
			self.le_storage_path.setText(str(new_path))

	@QtCore.Slot()
	def on_b_browse_default_clicked(self) -> bool:
		new_path = self.get_presets_dir()
		self.le_storage_path.setText(str(new_path))
		return True

	@QtCore.Slot()
	def on_b_browse_up_clicked(self) -> bool:
		pth = Path(self.le_storage_path.text()).parent
		self.le_storage_path.setText(str(pth))
		return True

	@QtCore.Slot()
	def on_le_storage_path_textChanged(self) -> bool:
		self._in_path = self.le_storage_path.text()
		self.refreshAll()
		return True

	# Filters
	def set_filters(self, glob_mode=True):
		flt = self.le_filter.text()
		flts, _, _ = self._tokenize2(flt, " ,;")
		if glob_mode:
			tt = []
			for x in flts:
				if not x.startswith("*"):
					x = f"*{x}"
				if not x.endswith("*"):
					x = f"{x}*"
				tt.append(x)
			flts = tt
		mdl = self.tv0.model()
		mdl.setNameFilters(flts)
		mdl.setNameFilterDisables(False)

	@QtCore.Slot()
	def on_le_filter_textChanged(self) -> bool:
		LOG.debug("on_le_filter_textChanged")
		self.set_filters()
		return True

	@QtCore.Slot()
	def on_b_clear_filter_clicked(self) -> bool:
		LOG.debug("on_b_clear_filter_clicked")
		self.le_filter.clear()
		return True

	###############  WINDOW CLASS STUFF
	def closeEvent(self, event):
		self.saveUI()
		self.close()

	def reject(self):
		"""Called when user presses Esc in QDialog.
		Returns:
		"""
		self.saveUI()
		super().reject()

	################ UI Store-Restore section ################
	def saveUI(self):
		"""
		QTableView: columns
		QCheckBox: state
		QLabel: text
		QLineEdit: text
		"""
		if not self.settings:
			return
		data = {}
		#
		LOG.debug("saving UI settings..")
		widget_name = self.objectName()
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		data = {}
		# checks
		for obj in checks:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.isChecked()
		# tables
		for obj in tables:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = []
			model = obj.model()
			if model is None:
				continue
			for i in range(0, model.columnCount()):
				val.append(obj.isColumnHidden(i))
			data[key] = val
		# labels
		for obj in labels:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.text()
		# lines
		for obj in lines:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.text()
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.toPlainText()
		# save window geo
		self.settings.setValue(widget_name, self.saveGeometry())
		# save settings dict as JSON string
		jdata = json.dumps(data)
		settings_key = f"{self.__class__.__name__}.{widget_name}"
		self.settings.setValue(settings_key, jdata)

	def loadUI(self):
		LOG.debug("loading UI settings..")
		widget_name = self.objectName()
		geo = self.settings.value(widget_name)
		if geo is not None:
			self.restoreGeometry(geo)
		settings_key = f"{self.__class__.__name__}.{widget_name}"
		jdata = self.settings.value(settings_key)
		if not jdata:
			return
		data = json.loads(jdata)
		if not data:
			return
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		# checks
		for obj in checks:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			if key not in data:
				continue
			val = data.get(key, None)
			if val is not None:
				state = QtCore.Qt.Checked
				if val is False:
					state = QtCore.Qt.Unchecked
				obj.setCheckState(state)
		# tables
		for obj in tables:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val:
				cnt = len(val)
				for i in range(0, cnt):
					obj.setColumnHidden(i, val[i])
		# labels
		for obj in labels:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# lines
		for obj in lines:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name or name == "":
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val:
				obj.document().setPlainText(val)
		pass

	@staticmethod
	def test():
		app = None
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py == "python":
			app = QtWidgets.QApplication(sys.argv)
		win1 = ClipWidget(ClipWidget.getParent(), name="test1")
		win1.show()
		# win2 = ClipWidget(ClipWidget.getParent(), name='test2')
		# win2.show()
		if py == "python":
			app.exec_()


class Clip(QtWidgets.QDialog, Ui_Clip):
	ORG = "aJoss"
	TITLE = "CG Clipboard"
	a_str_separator = ";"

	def __init__(self, parent=None):
		LOG.debug("start")
		LOG.debug("parent is: %s", parent)
		parent = parent or self.getParent()
		super().__init__(parent)
		self.setObjectName("Clip")
		#
		self._presets = {}  # presets name:Path dict
		self._tv_model = None
		#
		QtCore.QCoreApplication.setOrganizationDomain(self.TITLE)
		QtCore.QCoreApplication.setOrganizationName(self.TITLE)
		QtCore.QCoreApplication.setApplicationName(self.TITLE)
		self.settings = QtCore.QSettings()
		#
		self.initUI()
		LOG.debug("init done")

	@staticmethod
	def getParent():
		"""Returns parent window"""
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py in ("hython", "houdini", "houdinifx"):
			return hou.qt.mainWindow()

	def initUI(self):
		LOG.debug("init UI")
		self.setupUi(self)
		self.loadUI()

	def add_tab(self, name=None, path=None):
		if name is None:
			name = f"Clip{self.tabs.count()}"
		kw = {"parent": self, "name": name}
		if path is not None:
			kw["path"] = path
		wid = ClipWidget(**kw)
		self.tabs.addTab(wid, name)

	@QtCore.Slot()
	def on_b_add_tab_clicked(self) -> bool:
		self.add_tab()
		return True

	@QtCore.Slot()
	def on_b_remove_tab_clicked(self) -> bool:
		self.tabs.removeTab(self.tabs.currentIndex())
		return True

	###############  WINDOW CLASS STUFF
	def closeEvent(self, event):
		self.saveUI()
		self.close()

	def reject(self):
		"""Called when user presses Esc in QDialog.
		Returns:
		"""
		self.saveUI()
		super().reject()

	################ UI Store-Restore section ################
	def saveUI(self):
		"""
		QTableView: columns
		QCheckBox: state
		QLabel: text
		QLineEdit: text
		"""
		if not self.settings:
			return
		data = {}
		#
		LOG.debug("saving UI settings..")
		widget_name = self.objectName()
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		tabs = self.findChildren(QtWidgets.QTabWidget)
		data = {}
		# checks
		for obj in checks:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.isChecked()
		# tables
		for obj in tables:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = []
			model = obj.model()
			if model is None:
				continue
			for i in range(0, model.columnCount()):
				val.append(obj.isColumnHidden(i))
			data[key] = val
		# labels
		for obj in labels:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.text()
		# lines
		for obj in lines:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.text()
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			data[key] = obj.toPlainText()
		# tabs
		for obj in tabs:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			widgets = [obj.widget(x) for x in range(obj.count())]
			paths = [x._in_path for x in widgets]
			paths_str = self.a_str_separator.join(paths)
			data[key] = paths_str
		# save window geo
		self.settings.setValue(widget_name, self.saveGeometry())
		# save settings dict as JSON string
		jdata = json.dumps(data)
		settings_key = f"{self.__class__.__name__}.{widget_name}"
		self.settings.setValue(settings_key, jdata)

	def loadUI(self):
		LOG.debug("loading UI settings..")
		widget_name = self.objectName()
		geo = self.settings.value(widget_name)
		if geo is not None:
			self.restoreGeometry(geo)
		settings_key = f"{self.__class__.__name__}.{widget_name}"
		jdata = self.settings.value(settings_key)
		if not jdata:
			return
		data = json.loads(jdata)
		if not data:
			return
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		tabs = self.findChildren(QtWidgets.QTabWidget)
		# checks
		for obj in checks:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			if key not in data:
				continue
			val = data.get(key, None)
			if val is not None:
				state = QtCore.Qt.Checked
				if val is False:
					state = QtCore.Qt.Unchecked
				obj.setCheckState(state)
		# tables
		for obj in tables:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val:
				cnt = len(val)
				for i in range(0, cnt):
					obj.setColumnHidden(i, val[i])
		# labels
		for obj in labels:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# lines
		for obj in lines:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name or name == "":
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if val:
				obj.document().setPlainText(val)
		# tabs
		for obj in tabs:
			name = obj.objectName()
			if not name:
				continue
			key = f"{widget_name}.{name}"
			val = data.get(key, None)
			if not val:
				continue
			paths = val.split(self.a_str_separator)
			obj.clear()
			for p in paths:
				name = f"Clip{obj.count()}"
				wid = ClipWidget(parent=self, name=name, path=p)
				obj.addTab(wid, name)
		pass

	@staticmethod
	def test():
		app = None
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py == "python":
			app = QtWidgets.QApplication(sys.argv)
		win = Clip(Clip.getParent())
		win.show()
		if py == "python":
			app.exec_()


if __name__ == "__main__":
	Clip.test()
