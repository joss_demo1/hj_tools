# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'clip_widget_ui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_ClipboardWidget(object):
    def setupUi(self, ClipboardWidget):
        if not ClipboardWidget.objectName():
            ClipboardWidget.setObjectName(u"ClipboardWidget")
        ClipboardWidget.resize(704, 606)
        self.gridLayout = QGridLayout(ClipboardWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.hl_path1 = QHBoxLayout()
        self.hl_path1.setObjectName(u"hl_path1")
        self.le_storage_path = QLineEdit(ClipboardWidget)
        self.le_storage_path.setObjectName(u"le_storage_path")

        self.hl_path1.addWidget(self.le_storage_path)

        self.b_browse_up = QPushButton(ClipboardWidget)
        self.b_browse_up.setObjectName(u"b_browse_up")
        self.b_browse_up.setMaximumSize(QSize(24, 24))

        self.hl_path1.addWidget(self.b_browse_up)

        self.b_browse_default = QPushButton(ClipboardWidget)
        self.b_browse_default.setObjectName(u"b_browse_default")

        self.hl_path1.addWidget(self.b_browse_default)

        self.b_browse_path = QPushButton(ClipboardWidget)
        self.b_browse_path.setObjectName(u"b_browse_path")

        self.hl_path1.addWidget(self.b_browse_path)


        self.gridLayout.addLayout(self.hl_path1, 0, 0, 1, 1)

        self.gl_options2 = QGridLayout()
        self.gl_options2.setObjectName(u"gl_options2")
        self.cb_save_json_option = QCheckBox(ClipboardWidget)
        self.cb_save_json_option.setObjectName(u"cb_save_json_option")

        self.gl_options2.addWidget(self.cb_save_json_option, 0, 0, 1, 1)

        self.cb_save_hda_option = QCheckBox(ClipboardWidget)
        self.cb_save_hda_option.setObjectName(u"cb_save_hda_option")

        self.gl_options2.addWidget(self.cb_save_hda_option, 0, 1, 1, 1)


        self.gridLayout.addLayout(self.gl_options2, 1, 0, 1, 1)

        self.gl_buttons3 = QGridLayout()
        self.gl_buttons3.setObjectName(u"gl_buttons3")
        self.b_addfolder = QPushButton(ClipboardWidget)
        self.b_addfolder.setObjectName(u"b_addfolder")
        self.b_addfolder.setMinimumSize(QSize(0, 40))

        self.gl_buttons3.addWidget(self.b_addfolder, 2, 1, 1, 1)

        self.b_remove = QPushButton(ClipboardWidget)
        self.b_remove.setObjectName(u"b_remove")
        self.b_remove.setMinimumSize(QSize(0, 40))

        self.gl_buttons3.addWidget(self.b_remove, 2, 3, 1, 1)

        self.b_saveas = QPushButton(ClipboardWidget)
        self.b_saveas.setObjectName(u"b_saveas")
        self.b_saveas.setMinimumSize(QSize(0, 40))

        self.gl_buttons3.addWidget(self.b_saveas, 2, 2, 1, 1)

        self.b_expandall = QPushButton(ClipboardWidget)
        self.b_expandall.setObjectName(u"b_expandall")
        self.b_expandall.setMinimumSize(QSize(0, 40))

        self.gl_buttons3.addWidget(self.b_expandall, 2, 0, 1, 1)


        self.gridLayout.addLayout(self.gl_buttons3, 2, 0, 1, 1)

        self.hl_filter4 = QHBoxLayout()
        self.hl_filter4.setObjectName(u"hl_filter4")
        self.lb_filter = QLabel(ClipboardWidget)
        self.lb_filter.setObjectName(u"lb_filter")

        self.hl_filter4.addWidget(self.lb_filter)

        self.b_clear_filter = QPushButton(ClipboardWidget)
        self.b_clear_filter.setObjectName(u"b_clear_filter")

        self.hl_filter4.addWidget(self.b_clear_filter)

        self.le_filter = QLineEdit(ClipboardWidget)
        self.le_filter.setObjectName(u"le_filter")

        self.hl_filter4.addWidget(self.le_filter)


        self.gridLayout.addLayout(self.hl_filter4, 3, 0, 1, 1)

        self.gl_tv5 = QGridLayout()
        self.gl_tv5.setObjectName(u"gl_tv5")
        self.tv0 = QTreeView(ClipboardWidget)
        self.tv0.setObjectName(u"tv0")

        self.gl_tv5.addWidget(self.tv0, 0, 0, 1, 1)


        self.gridLayout.addLayout(self.gl_tv5, 4, 0, 1, 1)

        self.gl_buttons6 = QGridLayout()
        self.gl_buttons6.setObjectName(u"gl_buttons6")
        self.b_load = QPushButton(ClipboardWidget)
        self.b_load.setObjectName(u"b_load")
        self.b_load.setMinimumSize(QSize(0, 40))

        self.gl_buttons6.addWidget(self.b_load, 0, 1, 1, 1)


        self.gridLayout.addLayout(self.gl_buttons6, 5, 0, 1, 1)


        self.retranslateUi(ClipboardWidget)

        QMetaObject.connectSlotsByName(ClipboardWidget)
    # setupUi

    def retranslateUi(self, ClipboardWidget):
        ClipboardWidget.setWindowTitle(QCoreApplication.translate("ClipboardWidget", u"Clipboard", None))
        self.b_browse_up.setText(QCoreApplication.translate("ClipboardWidget", u"\u2191", None))
        self.b_browse_default.setText(QCoreApplication.translate("ClipboardWidget", u"Default", None))
        self.b_browse_path.setText(QCoreApplication.translate("ClipboardWidget", u"Browse", None))
        self.cb_save_json_option.setText(QCoreApplication.translate("ClipboardWidget", u"Save as JSON file", None))
        self.cb_save_hda_option.setText(QCoreApplication.translate("ClipboardWidget", u"Save HDA definitions", None))
        self.b_addfolder.setText(QCoreApplication.translate("ClipboardWidget", u"Add folder", None))
        self.b_remove.setText(QCoreApplication.translate("ClipboardWidget", u"Remove", None))
        self.b_saveas.setText(QCoreApplication.translate("ClipboardWidget", u"Save new preset", None))
        self.b_expandall.setText(QCoreApplication.translate("ClipboardWidget", u"Expand all", None))
        self.lb_filter.setText(QCoreApplication.translate("ClipboardWidget", u"Filter:", None))
        self.b_clear_filter.setText(QCoreApplication.translate("ClipboardWidget", u"Clear", None))
        self.b_load.setText(QCoreApplication.translate("ClipboardWidget", u"Load preset", None))
    # retranslateUi

