# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'clip_ui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Clip(object):
    def setupUi(self, Clip):
        if not Clip.objectName():
            Clip.setObjectName(u"Clip")
        Clip.resize(707, 705)
        self.gridLayout = QGridLayout(Clip)
        self.gridLayout.setObjectName(u"gridLayout")
        self.hl_buttons = QHBoxLayout()
        self.hl_buttons.setObjectName(u"hl_buttons")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hl_buttons.addItem(self.horizontalSpacer)

        self.b_add_tab = QPushButton(Clip)
        self.b_add_tab.setObjectName(u"b_add_tab")
        self.b_add_tab.setMaximumSize(QSize(24, 24))

        self.hl_buttons.addWidget(self.b_add_tab)

        self.b_remove_tab = QPushButton(Clip)
        self.b_remove_tab.setObjectName(u"b_remove_tab")
        self.b_remove_tab.setMaximumSize(QSize(24, 24))

        self.hl_buttons.addWidget(self.b_remove_tab)


        self.gridLayout.addLayout(self.hl_buttons, 0, 0, 1, 1)

        self.tabs = QTabWidget(Clip)
        self.tabs.setObjectName(u"tabs")

        self.gridLayout.addWidget(self.tabs, 1, 0, 1, 1)


        self.retranslateUi(Clip)

        self.tabs.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(Clip)
    # setupUi

    def retranslateUi(self, Clip):
        Clip.setWindowTitle(QCoreApplication.translate("Clip", u"Clipboard tool", None))
        self.b_add_tab.setText(QCoreApplication.translate("Clip", u"+", None))
        self.b_remove_tab.setText(QCoreApplication.translate("Clip", u"-", None))
    # retranslateUi

