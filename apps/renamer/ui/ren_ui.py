# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ren_ui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(532, 332)
        self.gridLayout_11 = QGridLayout(Widget)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.gl5 = QGridLayout()
        self.gl5.setObjectName(u"gl5")
        self.b_suffix = QPushButton(Widget)
        self.b_suffix.setObjectName(u"b_suffix")
        self.b_suffix.setMinimumSize(QSize(80, 0))
        self.b_suffix.setMaximumSize(QSize(80, 16777215))

        self.gl5.addWidget(self.b_suffix, 0, 0, 1, 1)

        self.le_suffix = QLineEdit(Widget)
        self.le_suffix.setObjectName(u"le_suffix")

        self.gl5.addWidget(self.le_suffix, 0, 1, 1, 1)


        self.gridLayout_11.addLayout(self.gl5, 4, 0, 1, 1)

        self.gl7 = QGridLayout()
        self.gl7.setObjectName(u"gl7")
        self.b_delete = QPushButton(Widget)
        self.b_delete.setObjectName(u"b_delete")
        self.b_delete.setMinimumSize(QSize(80, 0))
        self.b_delete.setMaximumSize(QSize(80, 16777215))

        self.gl7.addWidget(self.b_delete, 0, 0, 1, 1)

        self.le_delete = QLineEdit(Widget)
        self.le_delete.setObjectName(u"le_delete")

        self.gl7.addWidget(self.le_delete, 0, 1, 1, 1)

        self.gl7.setColumnStretch(0, 1)
        self.gl7.setColumnStretch(1, 3)

        self.gridLayout_11.addLayout(self.gl7, 6, 0, 1, 1)

        self.gl2 = QGridLayout()
        self.gl2.setObjectName(u"gl2")
        self.b_rename = QPushButton(Widget)
        self.b_rename.setObjectName(u"b_rename")
        self.b_rename.setMinimumSize(QSize(80, 0))
        self.b_rename.setMaximumSize(QSize(80, 16777215))

        self.gl2.addWidget(self.b_rename, 0, 0, 1, 1)

        self.le_rename = QLineEdit(Widget)
        self.le_rename.setObjectName(u"le_rename")

        self.gl2.addWidget(self.le_rename, 0, 1, 1, 1)

        self.le_rename_start = QLineEdit(Widget)
        self.le_rename_start.setObjectName(u"le_rename_start")

        self.gl2.addWidget(self.le_rename_start, 0, 2, 1, 1)

        self.gl2.setColumnStretch(0, 2)
        self.gl2.setColumnStretch(1, 3)
        self.gl2.setColumnStretch(2, 3)

        self.gridLayout_11.addLayout(self.gl2, 2, 0, 1, 1)

        self.gl0 = QGridLayout()
        self.gl0.setObjectName(u"gl0")
        self.b_select = QPushButton(Widget)
        self.b_select.setObjectName(u"b_select")
        self.b_select.setMinimumSize(QSize(80, 0))
        self.b_select.setMaximumSize(QSize(80, 16777215))

        self.gl0.addWidget(self.b_select, 0, 0, 1, 1)

        self.le_select = QLineEdit(Widget)
        self.le_select.setObjectName(u"le_select")

        self.gl0.addWidget(self.le_select, 0, 1, 1, 1)

        self.gl0.setColumnStretch(0, 1)
        self.gl0.setColumnStretch(1, 3)

        self.gridLayout_11.addLayout(self.gl0, 0, 0, 1, 1)

        self.gl4 = QGridLayout()
        self.gl4.setObjectName(u"gl4")
        self.b_prefix = QPushButton(Widget)
        self.b_prefix.setObjectName(u"b_prefix")
        self.b_prefix.setMinimumSize(QSize(80, 0))
        self.b_prefix.setMaximumSize(QSize(80, 16777215))

        self.gl4.addWidget(self.b_prefix, 0, 0, 1, 1)

        self.le_prefix = QLineEdit(Widget)
        self.le_prefix.setObjectName(u"le_prefix")

        self.gl4.addWidget(self.le_prefix, 0, 1, 1, 1)

        self.gl4.setColumnStretch(0, 1)
        self.gl4.setColumnStretch(1, 3)

        self.gridLayout_11.addLayout(self.gl4, 3, 0, 1, 1)

        self.gl6 = QGridLayout()
        self.gl6.setObjectName(u"gl6")
        self.le_replace_from = QLineEdit(Widget)
        self.le_replace_from.setObjectName(u"le_replace_from")

        self.gl6.addWidget(self.le_replace_from, 0, 1, 1, 1)

        self.b_replace = QPushButton(Widget)
        self.b_replace.setObjectName(u"b_replace")
        self.b_replace.setMinimumSize(QSize(80, 0))
        self.b_replace.setMaximumSize(QSize(80, 16777215))

        self.gl6.addWidget(self.b_replace, 0, 0, 1, 1)

        self.le_replace_to = QLineEdit(Widget)
        self.le_replace_to.setObjectName(u"le_replace_to")

        self.gl6.addWidget(self.le_replace_to, 0, 2, 1, 1)

        self.gl6.setColumnStretch(0, 2)
        self.gl6.setColumnStretch(1, 3)
        self.gl6.setColumnStretch(2, 3)

        self.gridLayout_11.addLayout(self.gl6, 5, 0, 1, 1)

        self.gl8 = QGridLayout()
        self.gl8.setObjectName(u"gl8")
        self.b_lower = QPushButton(Widget)
        self.b_lower.setObjectName(u"b_lower")

        self.gl8.addWidget(self.b_lower, 0, 0, 1, 1)

        self.b_upper = QPushButton(Widget)
        self.b_upper.setObjectName(u"b_upper")

        self.gl8.addWidget(self.b_upper, 0, 1, 1, 1)

        self.b_capital = QPushButton(Widget)
        self.b_capital.setObjectName(u"b_capital")

        self.gl8.addWidget(self.b_capital, 0, 2, 1, 1)


        self.gridLayout_11.addLayout(self.gl8, 7, 0, 1, 1)

        self.gl1 = QGridLayout()
        self.gl1.setObjectName(u"gl1")
        self.b_sel_bytype = QPushButton(Widget)
        self.b_sel_bytype.setObjectName(u"b_sel_bytype")

        self.gl1.addWidget(self.b_sel_bytype, 0, 0, 1, 1)

        self.b_sel_bytypename = QPushButton(Widget)
        self.b_sel_bytypename.setObjectName(u"b_sel_bytypename")

        self.gl1.addWidget(self.b_sel_bytypename, 0, 1, 1, 1)


        self.gridLayout_11.addLayout(self.gl1, 1, 0, 1, 1)

        self.gl9 = QGridLayout()
        self.gl9.setObjectName(u"gl9")
        self.b_reset = QPushButton(Widget)
        self.b_reset.setObjectName(u"b_reset")

        self.gl9.addWidget(self.b_reset, 0, 0, 1, 1)


        self.gridLayout_11.addLayout(self.gl9, 8, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_11.addItem(self.verticalSpacer, 9, 0, 1, 1)


        self.retranslateUi(Widget)

        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Renamer tool", None))
        self.b_suffix.setText(QCoreApplication.translate("Widget", u"Suffix", None))
        self.b_delete.setText(QCoreApplication.translate("Widget", u"Delete", None))
        self.b_rename.setText(QCoreApplication.translate("Widget", u"Sequence", None))
        self.le_rename_start.setText(QCoreApplication.translate("Widget", u"0", None))
        self.b_select.setText(QCoreApplication.translate("Widget", u"Select", None))
        self.b_prefix.setText(QCoreApplication.translate("Widget", u"Prefix", None))
        self.b_replace.setText(QCoreApplication.translate("Widget", u"Replace", None))
        self.b_lower.setText(QCoreApplication.translate("Widget", u"lower", None))
        self.b_upper.setText(QCoreApplication.translate("Widget", u"UPPER", None))
        self.b_capital.setText(QCoreApplication.translate("Widget", u"Capital", None))
        self.b_sel_bytype.setText(QCoreApplication.translate("Widget", u"by type", None))
        self.b_sel_bytypename.setText(QCoreApplication.translate("Widget", u"by type and name", None))
        self.b_reset.setText(QCoreApplication.translate("Widget", u"Reset all", None))
    # retranslateUi

