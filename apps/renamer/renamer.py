import fnmatch
import json
import logging
import sys
import os

# Setup logging
fmt = '%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"'
logging.basicConfig(level=logging.DEBUG, format=fmt)
LOG = logging.getLogger('Renamer')

from hj_tools.libs.initlib import initlib
Host = initlib.get_host()
#Qt = initlib.get_qt_mod()
Qt = initlib.get_mod('hj_tools.vendor.Qt')
from Qt import QtCore, QtGui, QtWidgets, QtCompat
LOG.debug(f'//----------------------\nHost: {Host}\nQt: {Qt}')


# Load Hou
try:
	import hou
except ImportError:
	LOG.debug('Houdini API is not loaded.')
	pass

from .ui.ren_ui import Ui_Widget


__all__ = ['Renamer']



class Renamer(QtWidgets.QDialog, Ui_Widget):
	ORG = 'aJoss'
	TITLE = 'Renamer'

	def __init__(self, parent=None):
		self.log = logging.getLogger(self.__class__.__name__)
		self.log.debug('Init.')
		self.log.debug('parent is: %s', parent)
		parent = parent or self.getParent()
		super(Renamer, self).__init__(parent)
		QtCore.QCoreApplication.setOrganizationDomain(self.TITLE)
		QtCore.QCoreApplication.setOrganizationName(self.TITLE)
		QtCore.QCoreApplication.setApplicationName(self.TITLE)
		self.settings = QtCore.QSettings()
		self.setObjectName("RenamerWidget")
		#
		self.initUI()
		self.initData()
		self.log.debug('Init done.')

	@staticmethod
	def getParent():
		"""Returns parent window"""
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py in ('hython', 'houdini', 'houdinifx'):
			return hou.qt.mainWindow()

	def initUI(self):
		self.setupUi(self)
		self.loadUI()

	def initData(self):
		self.refreshAll()

	def refreshAll(self):
		pass

	def stat_msg(self, msg=None):
		"""Emit app message (goes to statusbar and log)."""
		if msg is None:
			return
		# self.statusbar.showMessage(msg)
		self.log.debug(msg)

	########## Small library
	def ls(self, nodes=None, sl=False):
		"""
		List all/selected nodes in scene / from given nodes.

		:param nodes: if not empty, use nodes as multiple roots for subselections
		:param sl: bool, selected nodes only
		"""
		if sl:
			res = hou.selectedNodes()
		else:
			nodes = nodes or [hou.node('/')]
			lists = [x.allSubChildren() for x in nodes if isinstance(x, hou.Node)]
			res = [x for lst in lists for x in lst]
		return res

	def select(self, nodes=None):
		if not nodes:
			return
		nodes[0].setSelected(True, True)
		for node in nodes:
			node.setSelected(True, False)

	########## CLICK HANDLERS
	@QtCore.Slot()
	def on_b_select_clicked(self):
		mask = self.le_select.text() or '*'
		selnodes = [x for x in self.ls() if fnmatch.fnmatch(x.name(), mask)]
		self.select(selnodes)
		self.stat_msg('{} items selected'.format(len(selnodes)))
		return selnodes

	@QtCore.Slot()
	def on_b_sel_bytype_clicked(self):
		selnodes = self.ls(sl=True)
		if not selnodes:
			self.stat_msg('Nothing selected')
			return
		seltypes = list(set([x.type() for x in selnodes]))
		rnodes = []
		for i, typ in enumerate(seltypes):
			rnodes += typ.instances()
		self.select(rnodes)
		self.stat_msg('{} items selected'.format(len(rnodes)))
		return rnodes

	@QtCore.Slot()
	def on_b_sel_bytypename_clicked(self):
		selname = self.le_select.text()
		sel = self.ls(sl=True)
		seltypes = list(set([x.type() for x in sel]))
		typnodes = []
		for typ in seltypes:
			typnodes += typ.instances()
		rnodes = [x for x in typnodes if selname in x.name()]
		self.select(rnodes)
		self.stat_msg('{} items selected'.format(len(rnodes)))
		return rnodes

	@QtCore.Slot()
	def on_b_rename_clicked(self):
		nodes = self.ls(sl=True)
		newname = self.le_rename.text()
		if not newname:
			self.stat_msg('Empty new name given.')
			return
		startnum = 0
		try:
			startnum = int(self.le_rename_start.text())
		except ValueError:
			pass
		for i, node in enumerate(nodes):
			name = '{}_{}'.format(newname, startnum + i)
			self.log.debug('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_prefix_clicked(self):
		pre = self.le_prefix.text()
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = pre + node.name()
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_suffix_clicked(self):
		suff = self.le_suffix.text()
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name() + suff
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_replace_clicked(self):
		r_from = self.le_replace_from.text()
		r_to = self.le_replace_to.text()
		if not r_from:
			return
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name().replace(r_from, r_to)
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_delete_clicked(self):
		r_from = self.le_delete.text()
		r_to = ''
		if not r_from:
			return
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name().replace(r_from, r_to)
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_lower_clicked(self):
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name().lower()
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_upper_clicked(self):
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name().upper()
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_capital_clicked(self):
		nodes = self.ls(sl=True)
		for i, node in enumerate(nodes):
			name = node.name().capitalize()
			print('{} -> {}'.format(node.path(), name))
			node.setName(name)
		self.stat_msg('{} items renamed'.format(len(nodes)))

	@QtCore.Slot()
	def on_b_reset_clicked(self):
		self.le_select.setText('')
		self.le_rename.setText('')
		self.le_rename_start.setText('0')
		self.le_prefix.setText('')
		self.le_suffix.setText('')
		self.le_replace_from.setText('')
		self.le_replace_to.setText('')
		self.le_delete.setText('')

	###############  WINDOW CLASS STUFF
	def closeEvent(self, event):
		self.saveUI()
		self.close()

	################ UI Store-Restore section ################

	def saveUI(self):
		"""
		QTableView: columns
		QCheckBox: state
		QLabel: text
		QLineEdit: text
		"""
		if not self.settings:
			return
		data = {}
		#
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		data = {}
		# checks
		for obj in checks:
			name = obj.objectName()
			if not name: continue
			val = obj.isChecked()
			data[name] = val
		# tables
		for obj in tables:
			name = obj.objectName()
			if not name: continue
			val = []
			model = obj.model()
			if model is None: continue
			for i in range(0, model.columnCount()):
				val.append(obj.isColumnHidden(i))
			data[name] = val
		# labels
		for obj in labels:
			name = obj.objectName()
			if not name: continue
			val = obj.text()
			data[name] = val
		# lines
		for obj in lines:
			name = obj.objectName()
			if not name: continue
			val = obj.text()
			data[name] = val
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name: continue
			val = obj.toPlainText()
			data[name] = val

		# save window geo
		self.settings.setValue(self.objectName(), self.saveGeometry())
		# save settings dict
		jdata = json.dumps(data)
		self.settings.setValue('widgets', jdata)

	def loadUI(self):
		self.log.info('Loading UI settings..')
		geo = self.settings.value(self.objectName())
		if geo is not None:
			self.restoreGeometry(geo)
		jdata = self.settings.value('widgets')
		if not jdata:
			return
		data = json.loads(jdata)
		if not data:
			return
		checks = self.findChildren(QtWidgets.QCheckBox)
		tables = self.findChildren(QtWidgets.QTableView)
		labels = self.findChildren(QtWidgets.QLabel)
		lines = self.findChildren(QtWidgets.QLineEdit)
		plaintextedits = self.findChildren(QtWidgets.QPlainTextEdit)
		# checks
		for obj in checks:
			name = obj.objectName()
			if name not in data:
				continue
			val = dict.get(name, None)
			if val is not None:
				state = QtCore.Qt.Checked
				if val is False:
					state = QtCore.Qt.Unchecked
				obj.setCheckState(state)
		# tables
		for obj in tables:
			name = obj.objectName()
			val = data.get(name, None)
			if val:
				cnt = len(val)
				for i in range(0, cnt):
					obj.setColumnHidden(i, val[i])
		# labels
		for obj in labels:
			name = obj.objectName()
			val = dict.get(name, None)
			if val:
				obj.setText(val)
		# lines
		for obj in lines:
			name = obj.objectName()
			val = data.get(name, None)
			if val:
				obj.setText(val)
		# plain text editors
		for obj in plaintextedits:
			name = obj.objectName()
			if not name or name == '':
				continue
			val = data.get(name, None)
			if val:
				obj.document().setPlainText(val)
		pass

	@staticmethod
	def test():
		app = None
		host = initlib.get_host()
		if host == initlib.STANDALONE:
			app = QtWidgets.QApplication(sys.argv)
		win = Renamer(Renamer.getParent())
		win.show()
		if host == initlib.STANDALONE:
			app.exec_()



if __name__ == "__main__":
	Renamer.test()
