# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'envexp_ui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(783, 769)
        self.gridLayout = QGridLayout(Widget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gl_pane = QGridLayout()
        self.gl_pane.setObjectName(u"gl_pane")
        self.gl_toolbar = QGridLayout()
        self.gl_toolbar.setObjectName(u"gl_toolbar")
        self.b_expand = QPushButton(Widget)
        self.b_expand.setObjectName(u"b_expand")

        self.gl_toolbar.addWidget(self.b_expand, 0, 0, 1, 1)

        self.b_collapse = QPushButton(Widget)
        self.b_collapse.setObjectName(u"b_collapse")

        self.gl_toolbar.addWidget(self.b_collapse, 0, 1, 1, 1)

        self.b_explore = QPushButton(Widget)
        self.b_explore.setObjectName(u"b_explore")

        self.gl_toolbar.addWidget(self.b_explore, 0, 2, 1, 1)


        self.gl_pane.addLayout(self.gl_toolbar, 0, 0, 1, 1)

        self.gl_filter = QGridLayout()
        self.gl_filter.setObjectName(u"gl_filter")
        self.le_filter = QLineEdit(Widget)
        self.le_filter.setObjectName(u"le_filter")

        self.gl_filter.addWidget(self.le_filter, 0, 1, 1, 1)

        self.b_clear_filter = QPushButton(Widget)
        self.b_clear_filter.setObjectName(u"b_clear_filter")

        self.gl_filter.addWidget(self.b_clear_filter, 0, 0, 1, 1)

        self.gl_filter.setColumnStretch(0, 1)
        self.gl_filter.setColumnStretch(1, 5)

        self.gl_pane.addLayout(self.gl_filter, 1, 0, 1, 1)

        self.gl_view = QGridLayout()
        self.gl_view.setObjectName(u"gl_view")
        self.tree_evars = QTreeView(Widget)
        self.tree_evars.setObjectName(u"tree_evars")

        self.gl_view.addWidget(self.tree_evars, 0, 0, 1, 1)


        self.gl_pane.addLayout(self.gl_view, 2, 0, 1, 1)


        self.gridLayout.addLayout(self.gl_pane, 0, 0, 1, 1)

        self.gl_pane_2 = QGridLayout()
        self.gl_pane_2.setObjectName(u"gl_pane_2")
        self.gl_toolbar_2 = QGridLayout()
        self.gl_toolbar_2.setObjectName(u"gl_toolbar_2")
        self.b_expand_2 = QPushButton(Widget)
        self.b_expand_2.setObjectName(u"b_expand_2")

        self.gl_toolbar_2.addWidget(self.b_expand_2, 0, 0, 1, 1)

        self.b_collapse_2 = QPushButton(Widget)
        self.b_collapse_2.setObjectName(u"b_collapse_2")

        self.gl_toolbar_2.addWidget(self.b_collapse_2, 0, 1, 1, 1)

        self.b_explore_2 = QPushButton(Widget)
        self.b_explore_2.setObjectName(u"b_explore_2")

        self.gl_toolbar_2.addWidget(self.b_explore_2, 0, 2, 1, 1)


        self.gl_pane_2.addLayout(self.gl_toolbar_2, 0, 0, 1, 1)

        self.gl_filter_2 = QGridLayout()
        self.gl_filter_2.setObjectName(u"gl_filter_2")
        self.le_filter_2 = QLineEdit(Widget)
        self.le_filter_2.setObjectName(u"le_filter_2")

        self.gl_filter_2.addWidget(self.le_filter_2, 0, 1, 1, 1)

        self.b_clear_filter_2 = QPushButton(Widget)
        self.b_clear_filter_2.setObjectName(u"b_clear_filter_2")

        self.gl_filter_2.addWidget(self.b_clear_filter_2, 0, 0, 1, 1)

        self.gl_filter_2.setColumnStretch(0, 1)
        self.gl_filter_2.setColumnStretch(1, 5)

        self.gl_pane_2.addLayout(self.gl_filter_2, 1, 0, 1, 1)

        self.gl_view_2 = QGridLayout()
        self.gl_view_2.setObjectName(u"gl_view_2")
        self.tree_evars_2 = QTreeView(Widget)
        self.tree_evars_2.setObjectName(u"tree_evars_2")

        self.gl_view_2.addWidget(self.tree_evars_2, 0, 0, 1, 1)


        self.gl_pane_2.addLayout(self.gl_view_2, 2, 0, 1, 1)


        self.gridLayout.addLayout(self.gl_pane_2, 0, 1, 1, 1)


        self.retranslateUi(Widget)

        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Clipboard tool", None))
        self.b_expand.setText(QCoreApplication.translate("Widget", u"Expand", None))
        self.b_collapse.setText(QCoreApplication.translate("Widget", u"Collapse", None))
        self.b_explore.setText(QCoreApplication.translate("Widget", u"Explorer", None))
        self.b_clear_filter.setText(QCoreApplication.translate("Widget", u"Clear", None))
        self.b_expand_2.setText(QCoreApplication.translate("Widget", u"Expand", None))
        self.b_collapse_2.setText(QCoreApplication.translate("Widget", u"Collapse", None))
        self.b_explore_2.setText(QCoreApplication.translate("Widget", u"Explorer", None))
        self.b_clear_filter_2.setText(QCoreApplication.translate("Widget", u"Clear", None))
    # retranslateUi

