import logging
import os
import subprocess
import sys

# Logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"')
LOG = logging.getLogger("EnvExp")
# Bootstrap Qt
from hj_tools.libs.initlib import initlib

Host = initlib.get_host()
Qt = initlib.get_qt_mod()
from Qt import QtCompat, QtCore, QtGui, QtWidgets

LOG.debug(f"//----------------------\nHost: {Host}\nQt: {Qt}")
# Hou
try:
	import hou
except ImportError:
	LOG.debug("Houdini API is not loaded.")
	pass


class Envexp(QtWidgets.QMainWindow):
	NAME = "environmentExplorer"
	TITLE = "Environment Explorer"
	_parent = None
	_key_control = False
	_key_alt = False
	_key_shift = False
	# QListWidgets states:
	sel_name = None
	sel_value = None
	sel_file = None
	# strings:
	no_path = ["Path does not exist, nothing to list"]

	def __init__(self, parent=None):
		self.parent = parent
		if self._parent is None:
			self._parent = self.getParent()
		super(Envexp, self).__init__(parent=self._parent)
		self.initStyle()
		self.initUI()
		self.initData()

	@staticmethod
	def getParent():
		"""Returns parent window"""
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		if py in ("hython", "houdini", "houdinifx"):
			return hou.qt.mainWindow()

	def initStyle(self):
		"""
		Trying to load qss stylesheet
		"""
		fstyle = "jss1.qss"
		cdir = os.path.dirname(os.path.realpath(__file__))
		style = os.path.join(cdir, fstyle)
		if not os.path.exists(style):
			return False
		with open(style, "r") as fh:
			self.setStyleSheet(fh.read())
			self.statusMsg(fstyle + " is set as the main stylesheet")
		return True

	def initUI(self):
		# vars
		w = 1200
		h = 900
		b8040 = QtCore.QSize(80, 40)
		b6030 = QtCore.QSize(60, 30)
		b6020 = QtCore.QSize(60, 20)
		#
		self.setWindowFlags(QtCore.Qt.Window)
		self.setWindowTitle(self.TITLE)
		self.setObjectName(self.NAME)
		self.resize(w, h)
		self.centralwidget = QtWidgets.QWidget(self)
		# UI Begin
		self.vl_main = QtWidgets.QVBoxLayout(self.centralwidget)
		self.vl_main.setContentsMargins(2, 2, 2, 2)
		self.vl_main.setSpacing(0)
		# Top toolbar
		self.hl_btns = QtWidgets.QHBoxLayout()
		self.hl_btns.setContentsMargins(2, 2, 2, 2)
		self.pb_1 = QtWidgets.QPushButton()
		self.pb_1.setMinimumSize(b6030)
		self.pb_2 = QtWidgets.QPushButton()
		self.pb_2.setMinimumSize(b6030)
		self.pb_3 = QtWidgets.QPushButton()
		self.pb_3.setMinimumSize(b6030)
		spacer_b1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		self.hl_btns.addWidget(self.pb_1)
		self.hl_btns.addWidget(self.pb_2)
		self.hl_btns.addWidget(self.pb_3)
		self.hl_btns.addItem(spacer_b1)
		# Top left and right toolbars
		self.hl_top = QtWidgets.QHBoxLayout()
		# top left buttons
		self.hl_top_left = QtWidgets.QHBoxLayout()
		self.pb_left_tool1 = QtWidgets.QPushButton()
		self.pb_left_tool2 = QtWidgets.QPushButton()
		spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		self.hl_top_left.addWidget(self.pb_left_tool1)
		self.hl_top_left.addWidget(self.pb_left_tool2)
		self.hl_top_left.addItem(spacerItem)
		# top right buttons
		self.hl_top_right = QtWidgets.QHBoxLayout()
		spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
		self.pb_right_tool1 = QtWidgets.QPushButton()
		self.pb_right_tool2 = QtWidgets.QPushButton()
		self.hl_top_right.addItem(spacerItem1)
		self.hl_top_right.addWidget(self.pb_right_tool1)
		self.hl_top_right.addWidget(self.pb_right_tool2)
		#
		self.hl_top.addLayout(self.hl_top_left)
		self.hl_top.addLayout(self.hl_top_right)
		# Env listers layout
		self.vl_env = QtWidgets.QVBoxLayout()
		self.vl_env.setContentsMargins(2, 2, 2, 2)
		self.vl_env.setSpacing(0)
		# Splitter
		self.splitter = QtWidgets.QSplitter()
		self.splitter.setOrientation(QtCore.Qt.Horizontal)
		# self.splitter.setStretchFactor(0, 3)
		# self.splitter.setStretchFactor(1, 1)
		# FilterEnv line
		# Using widget as a wrapper for layout, so it can be added to stretch
		self.wdg_filterEnv = QtWidgets.QWidget()
		self.vl_filterEnv = QtWidgets.QVBoxLayout()
		self.hl_filterEnv = QtWidgets.QHBoxLayout()
		self.qle_filterEnv = QtWidgets.QLineEdit()
		self.pb_filterEnvClear = QtWidgets.QPushButton()
		self.pb_filterEnvClear.setMinimumSize(b6020)
		self.hl_filterEnv.addWidget(self.qle_filterEnv)
		self.hl_filterEnv.addWidget(self.pb_filterEnvClear)
		# QTreeView
		# Creating model right here
		self.tv_model = QtGui.QStandardItemModel()
		self.qtv_env = QtWidgets.QTreeView(self.wdg_filterEnv)
		self.qtv_env.setModel(self.tv_model)
		self.qtv_env.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		self.qtv_env.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
		self.qtv_env.setUniformRowHeights(True)
		self.qtv_env.setExpandsOnDoubleClick(True)
		self.qtv_env.sizePolicy().setHorizontalStretch(3)
		#
		self.vl_filterEnv.addLayout(self.hl_filterEnv)
		self.vl_filterEnv.addWidget(self.qtv_env)
		self.wdg_filterEnv.setLayout(self.vl_filterEnv)
		# FilterVal line
		self.wdg_filterVal = QtWidgets.QWidget()
		self.vl_filterVal = QtWidgets.QVBoxLayout()
		self.hl_filterVal = QtWidgets.QHBoxLayout()
		self.qle_filterVal = QtWidgets.QLineEdit()
		self.pb_filterValClear = QtWidgets.QPushButton()
		self.pb_filterValClear.setMinimumSize(b6020)
		self.hl_filterVal.addWidget(self.qle_filterVal)
		self.hl_filterVal.addWidget(self.pb_filterValClear)
		# Lister
		self.qlw_lister = QtWidgets.QListWidget(self.wdg_filterVal)
		self.qlw_lister.sizePolicy().setHorizontalStretch(1)
		#
		self.vl_filterVal.addLayout(self.hl_filterVal)
		self.vl_filterVal.addWidget(self.qlw_lister)
		self.wdg_filterVal.setLayout(self.vl_filterVal)
		# add everything to splitter
		self.splitter.addWidget(self.wdg_filterEnv)
		self.splitter.addWidget(self.wdg_filterVal)
		# setting layouts
		self.vl_env.addWidget(self.splitter)
		self.vl_main.addLayout(self.hl_btns)
		self.vl_main.addLayout(self.hl_top)
		self.vl_main.addLayout(self.vl_env)
		# finalize
		# self.centralwidget.setLayout(self.vl_main)
		self.setCentralWidget(self.centralwidget)
		# status
		self.statusbar = QtWidgets.QStatusBar(self)
		self.setStatusBar(self.statusbar)
		# menu
		self.menubar = QtWidgets.QMenuBar(self)
		# File menu
		self.menu_File = QtWidgets.QMenu(self.menubar)
		self.action_Open = QtWidgets.QAction(self)
		self.action_Save = QtWidgets.QAction(self)
		self.action_Saveas = QtWidgets.QAction(self)
		self.action_Exit = QtWidgets.QAction(self)
		self.menu_File.addAction(self.action_Open)
		self.menu_File.addAction(self.action_Save)
		self.menu_File.addAction(self.action_Saveas)
		self.menu_File.addSeparator()
		self.menu_File.addAction(self.action_Exit)
		# About menu
		self.menu_Help = QtWidgets.QMenu(self.menubar)
		self.action_About = QtWidgets.QAction(self)
		self.menu_Help.addAction(self.action_About)
		self.menubar.addAction(self.menu_File.menuAction())
		self.menubar.addAction(self.menu_Help.menuAction())
		self.setMenuBar(self.menubar)
		# UI is done here
		self.initUI2()
		self.initUI3()

	def initUI2(self):
		"""
		Separate initialization of all the labels
		"""
		self.pb_1.setText("Expand")
		self.pb_2.setText("Collapse")
		self.pb_3.setText("Explorer")
		self.pb_filterEnvClear.setText("Clear")
		self.pb_filterValClear.setText("Clear")
		self.menu_File.setTitle("&File")
		self.menu_Help.setTitle("&Help")
		self.action_Open.setText("&Open")
		self.action_Save.setText("&Save")
		self.action_Saveas.setText("Save &as")
		self.action_Exit.setText("E&xit")
		self.action_About.setText("&About")
		self.statusMsg("initUI() is done")

	def initUI3(self):
		"""
		Signals and slots
		"""
		self.qtv_env.clicked.connect(self.b_clickTree)
		self.pb_1.clicked.connect(self.env_expand)
		self.pb_2.clicked.connect(self.env_collapse)
		self.pb_3.clicked.connect(self.env_fileman)
		self.qle_filterEnv.textChanged.connect(self.flt_changed)
		self.qle_filterVal.textChanged.connect(self.flt_changed)
		self.pb_filterEnvClear.clicked.connect(self.fltEnv_clear)
		self.pb_filterValClear.clicked.connect(self.fltVal_clear)

	############### Initial data
	def initData(self):
		self.refreshAll()

	################## REFRESHERS
	def refreshAll(self):
		""" """
		self.refreshTree()

	def refreshTree(self):
		"""
		TODO: Check values for existance and just set the red background brush for bad QStandardItems
		TODO: Move processing from refreshLister to here: processing %VAR%, etc
		"""
		edict = dict(os.environ)
		self.qtv_env.setUpdatesEnabled(False)
		self.tv_model.clear()
		self.tv_model.setHorizontalHeaderLabels(["Env"])
		for k in sorted(edict.keys()):
			evar = k
			evalue = edict[k]
			if not self.flt_key_filter(evar):
				continue
			evalues = sorted(evalue.split(os.pathsep))
			root = QtGui.QStandardItem("{}".format(evar))
			for val in evalues:
				if not val or val == "" or not len(val) or not self.flt_val_filter(val):
					continue
				child = QtGui.QStandardItem("{}".format(val))
				root.appendRow(child)
			if not root.hasChildren():
				continue
			self.tv_model.appendRow(root)
		self.qtv_env.setUpdatesEnabled(True)

	def refreshLister(self):
		"""Trying to detect what we've got and display the contents: files, envVars, etc. Could use some filter for files as well"""
		sel = self.qtv_env.selectedIndexes()
		if not len(sel):
			return
		item = sel[0]
		value = item.data()
		# processing given value
		# %VAR% check, expanding if any
		if value.endswith(":"):
			value = os.path.join(value, os.sep)
		if "%" in value:
			value = value.replace("%", "")
			value = os.environ.get(value, self.no_path)
		# now directory check
		items = []
		if not os.path.isdir(value):
			items = self.no_path
		else:
			items = os.listdir(value)
		self.qlw_lister.clear()
		self.qlw_lister.addItems(items)

	########## CLICK HANDLERS
	def b_clickTree(self):
		self.refreshLister()

	def env_expand(self):
		self.qtv_env.expandAll()

	def env_collapse(self):
		self.qtv_env.collapseAll()

	def env_fileman(self):
		sel = self.qtv_env.selectedIndexes()
		if not len(sel):
			return
		item = sel[0]
		value = item.data()
		if os.path.exists(value) and os.path.isdir(value):
			self.fileman(value)

	# Filter handlers
	def flt_changed(self):
		self.refreshTree()

	def fltEnv_clear(self):
		self.qle_filterEnv.clear()

	def fltVal_clear(self):
		self.qle_filterVal.clear()

	def flt_key_filter(self, instr):
		"""Helper func to filter values out, returns True/False"""
		flt = self.qle_filterEnv.text()
		if not len(flt):
			return True
		if flt.lower() in instr.lower():
			return True
		return False

	def flt_val_filter(self, instr):
		"""Helper func to filter values out, returns True/False"""
		flt = self.qle_filterVal.text()
		if not len(flt):
			return True
		if flt.lower() in instr.lower():
			return True
		return False

	###############  WINDOW CLASS STUFF
	def run(self):
		self.show()

	def stop(self):
		self.close()

	# keyboard
	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Control:
			self._key_control = True
		elif event.key() == QtCore.Qt.Key_Alt:
			self._key_alt = True
		elif event.key() == QtCore.Qt.Key_Shift:
			self._key_shift = True
		event.accept()

	def keyReleaseEvent(self, event):
		if event.key() == QtCore.Qt.Key_Control:
			self._key_control = False
		elif event.key() == QtCore.Qt.Key_Alt:
			self._key_alt = False
		elif event.key() == QtCore.Qt.Key_Shift:
			self._key_shift = False
		event.accept()

	def statusMsg(self, msg=None):
		if msg is None:
			return
		self.statusbar.showMessage(msg)

	def msgBox(self, msg):
		QtWidgets.QMessageBox.warning(self, "Message", msg, QtWidgets.QMessageBox.Ok)

	def fileOpenDialog(self, path=None):
		pth = path
		if pth is None:
			pth = "c:/"
		options = QtWidgets.QFileDialog.Options()
		# options |= QtGui.QFileDialog.DontUseNativeDialog
		fname, flt = QtWidgets.QFileDialog.getOpenFileName(None, "Open JSON", pth, "JSON Files (*.json);;All Files (*);;")
		fname = unicode(fname)
		if fname:
			return fname
		return None

	def fileman(self, path):
		"""
		Opens system file manager at clicked path
		"""
		if not os.path.isdir(path):
			self.statusMsg("{} is not a folder".format(path))
			return
		cmd = None
		osys = sys.platform
		if osys == "win32":
			cmd = r"explorer.exe "
		elif osys == "linux":
			cmd = r"xdg-open "
		cmd += path
		# cmd = cmd.decode('utf-8')
		subprocess.Popen(cmd)

	def clipcopy(self, text):
		clip = Qt.QApplication.clipboard()
		clip.setText(text)

	@staticmethod
	def test():
		app = None
		py = os.path.splitext(os.path.basename(sys.executable))[0]
		is_standalone = False if py in ("hython", "houdini", "houdinifx") else True
		if is_standalone:
			app = QtWidgets.QApplication(sys.argv)
		win = Envexp()
		win.run()
		if is_standalone:
			app.exec_()


if __name__ == "__main__":
	Envexp.test()
