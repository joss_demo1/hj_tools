
=== Houdini python libs ==========================================

  "hjtools.py" is visible to Houdini from the very beginning and
  bootstrapping the whole module by adding it to the sys.path and
  importing the starter functions.