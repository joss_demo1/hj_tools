#### HJ Tools: Houdini Joss Tools

## What is it?

**hj_tools** is a set of various Python/Qt Houdini tools combined into a single Python module.

Right now it consists of:


- Renamer tool
	
	* Allows for mass-selection and renaming of hundreds of nodes in one click.
	* Useful for crowds and other massive setups.


- Clipboard tool

	* Tool that allows you to store selected nodes to a separate file.
	* It's a kind of asset manager allowing Houdini artists to quickly save/load pieces of scenes, saving significant amount of clicks every day.
	* Multitab UI is supported so users can work with multiple library locations at once.


- EnvExp tool

	* Quick debugging tool showing current environment in a tree-like view.


## Installation

  - Clone the repository
  - Add a line to your houdini.env:

  ```
    HOUDINI_PATH = "C:/your_path/hj_tools;&"
  ```
  - Launch Houdini, you should have new "Tools" menu
  - Optionally you can add a 2nd line: `PYTHONPATH = "C:/your_path/;&"`

  ![Menu](docs/0_menu.png)
	

## Usage

### Clip

![Clip](docs/1_clip.png)

Clip tool allows user to save and load pieces of his nodes network.  
It works with different type of networks, so everything, including materials can be saved.  


### Envexp

![Envexp](docs/2_envexp.png)

Envexp is a debugging tool showing you all environment variables and paths contents.


### Renamer

![Renamer](docs/3_renamer.png)

Renamer is a tool made for mass-renaming things in Houdini.
It can be useful for massive setups, crowds for instance where you have dozens of nodes for each animation,
so manual renaming is just a waste of time but user still want it to keep clutter away.
