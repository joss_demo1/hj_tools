# -*- coding: utf-8 -*-
import inspect
import logging
import pprint
import sys
from pathlib import Path

sys.dont_write_bytecode = True
if sys.version_info.major >= 3:
	from importlib import reload
if not logging.getLogger().hasHandlers():
	fmt = '%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"'
	# logging.basicConfig(level=logging.DEBUG, format=fmt, filename=f'{__name__}.log', filemode="w+")
	logging.basicConfig(level=logging.DEBUG, format=fmt)
LOG = logging.getLogger(__name__)
CFL = Path(inspect.getsourcefile(lambda: 0)).resolve()
CWD = CFL.parent


def _paths(pathlist):
	for path in pathlist:
		p = (CWD / path).resolve()
		if not p.exists() or str(p) in sys.path:
			continue
		LOG.debug(f'Add "{p}" to sys.path')
		sys.path.append(str(p))
_paths([".."])


def start():
	# start_renamer()
	hclip()
	# start_envexp()


def test():
	pass


def hclip():
	from hj_tools.apps.clip.clip import Clip
	Clip.test()


def hrenamer():
	from hj_tools.apps.renamer.renamer import Renamer
	Renamer.test()


def henvexp():
	from hj_tools.apps.envexp.envexp import Envexp
	Envexp.test()


LOG.info(f"hj_tools imported Ok.")

if __name__ == "__main__":
	start()
