# -*- coding: utf-8 -*-

import logging
from pathlib import Path
import sys


sys.dont_write_bytecode = True
LOG = logging.getLogger(__name__)



def _paths(pathlist):
	cwd = Path(__file__).resolve().parent
	for path in pathlist:
		p = (cwd / path).resolve()
		if not p.exists() or str(p) in sys.path:
			continue
		print(f'Add "{p}" to sys.path')
		sys.path.append(str(p))
# Expose level above to make . a importable module
_paths(['.'])
