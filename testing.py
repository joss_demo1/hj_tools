""" Testing stub """

import inspect
import logging
from pathlib import Path
import sys


fmt = '%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"'
LOG = logging.getLogger(__name__)
CWD = Path(inspect.getsourcefile(lambda: 0)).resolve().parent
CFL = Path(inspect.getsourcefile(lambda: 0)).resolve()
logging.basicConfig(level=logging.DEBUG, format=fmt)#, filename=f'{__name__}.log', filemode="w+")
#logging.basicConfig(level=logging.DEBUG, format=fmt, handlers=[logging.FileHandler(LOG_FILE), logging.StreamHandler()])


def _paths(pathlist):
	for path in pathlist:
		p = (CWD / path).resolve()
		if not p.exists() or str(p) in sys.path:
			continue
		LOG.debug(f'Add "{p}" to sys.path')
		sys.path.append(str(p))
# Expose 1 level above to get access to the root module
_paths(['..'])
