from __future__ import (absolute_import, division, print_function, unicode_literals)

import logging
import os
import pprint
import sys
from importlib import import_module

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"')
LOG = logging.getLogger(__name__)


################################################################################
# Initlib has to be here for the proper initialization
class initlib(object):
	"""A small helper class to initialize the host application and get the host Qt module."""

	BLENDER = "blender"
	C4D = "c4d"
	HOUDINI = "houdini"
	MAX = "3dsmax"
	MAYA = "maya"
	MAYAPY = "mayapy"
	MAYABATCH = "mayabatch"
	MARI = "mari"
	NUKE = "nuke"
	KATANA = "katana"
	UE5 = "ue5"
	STANDALONE = "standalone"
	MODULES = {
		BLENDER: "bpy",
		C4D: "c4d.gui",
		HOUDINI: "hou",
		MARI: "mari",
		MAX: "MaxPlus",
		MAYA: "maya",
		NUKE: "nuke.utils",
		KATANA: "UI4",
		UE5: "unreal",
	}

	@staticmethod
	def get_mod(name=None, log=False):
		try:
			mod = import_module(name)
		except ImportError as e:
			if log:
				LOG.info("get_mod(): {}".format(e))
			mod = None
		return mod

	@staticmethod
	def get_qt_mod(log=False):
		return initlib.get_mod("Qt", log=log)

	@staticmethod
	def get_maya_mode():
		"""Determine the current mode of Maya."""
		try:
			import maya.cmds as mc
		except:
			return initlib.STANDALONE
		try:
			import maya.standalone
			maya.standalone.initialize()
			return initlib.MAYAPY
		except:
			pass
		if mc.about(batch=True):
			return initlib.MAYABATCH
		else:
			return initlib.MAYA

	@staticmethod
	def get_host():
		host = initlib.STANDALONE
		for name, hostmod in initlib.MODULES.items():
			mod = initlib.get_mod(hostmod)
			if mod is not None:
				host = name
				if host == initlib.MAYA:
					host = initlib.get_maya_mode()
				break
		return host

	@staticmethod
	def get_host_mod(init=True):
		mod = None
		host = initlib.get_host()
		hostmod = initlib.MODULES.get(host)
		if hostmod:
			mod = initlib.get_mod(hostmod)
		if init:
			if host == initlib.MAYA:
				try:
					import maya.standalone

					maya.standalone.initialize()
				except:
					pass
		return mod

	@staticmethod
	def is_gui():
		gui_mode = False
		host = initlib.get_host()
		mod = initlib.get_host_mod()
		if mod is None:
			if host == initlib.STANDALONE:
				return True
			return False
		if host == initlib.HOUDINI:
			gui_mode = mod.isUIAvailable()
		elif host == initlib.MAYA:
			# import maya.api.OpenMaya as om
			gui_mode = mod.api.OpenMaya.MGlobal.mayaState() == mod.api.OpenMaya.MGlobal.kInteractive
		# gui_mode = not mod.cmds.about(batch=True)
		elif host == initlib.NUKE:
			gui_mode = mod.env.get("gui")
		return gui_mode

	@staticmethod
	def is_gui2():
		gui_mode = False
		mod = initlib.get_mod("Qt.QWidgets")
		if mod is None:
			return gui_mode
		app = mod.QApplication.instance()
		gui_mode = app is not None
		return gui_mode

	@staticmethod
	def get_top_widget(name):
		qtwidgets = initlib.get_mod("Qt.QtWidgets")
		if not qtwidgets:
			return None
		for widget in qtwidgets.QApplication.topLevelWidgets():
			if widget.objectName() == name:
				return widget
		return None

	@staticmethod
	def get_main_window():
		if 0:
			qtw = initlib.get_mod("Qt.QtWidgets")
			app = initlib.qapp()
			for widget in app.topLevelWindows():
				if isinstance(widget, qtw.QMainWindow):
					return widget
			return None
		widget = None
		if initlib.is_gui():
			mod = initlib.get_host_mod()
			host = initlib.get_host()
			if host == initlib.HOUDINI:
				widget = mod.qt.mainWindow()
			elif host == initlib.MAYA:
				widget = initlib.get_top_widget("MayaWindow")
			elif host == initlib.NUKE:
				widget = initlib.get_top_widget("Foundry::UI::DockMainWindow")
			elif host == initlib.MAX:
				widget = mod.GetQMaxMainWindow()
			elif host == initlib.KATANA:
				mod.app.activateMainWindow()
				widget = mod.activeWindow()
			elif host == initlib.KATANA:
				# TODO: 2 Katanas?
				widget = mod.App.MainWindow.GetMainWindow()
		return widget

	@staticmethod
	def get_info():
		res = {}
		res["HOST"] = initlib.get_host()
		res["Qt"] = initlib.get_qt_mod()
		return res

	@staticmethod
	def print_info():
		res = []
		dt = initlib.get_info()
		for k, v in dt.items():
			res.append("{}: {}".format(k, v))
		LOG.info("\n".join(res))

	@staticmethod
	def qapp():
		qtw = initlib.get_mod("Qt.QtWidgets")
		if not qtw:
			return None
		app = qtw.QApplication.instance()
		if not app:
			app = qtw.QApplication([])
		return app

	@staticmethod
	def test():
		initlib.print_info()
		LOG.info("is_gui: {}".format(initlib.is_gui()))
		LOG.info("is_gui2: {}".format(initlib.is_gui2()))
		LOG.info("get_main_window: {}".format(initlib.get_main_window()))
		LOG.info("get_host_mod: {}".format(initlib.get_host_mod()))
		LOG.info("get_host: {}".format(initlib.get_host()))
		LOG.info("get_qt_mod: {}".format(initlib.get_qt_mod()))
		LOG.info("get_maya_mode: {}".format(initlib.get_maya_mode()))
		LOG.info("get_mod: {}".format(initlib.get_mod("Qt.QtWidgets")))
		LOG.info("get_top_widget: {}".format(initlib.get_top_widget("MayaWindow")))
		LOG.info("qapp: {}".format(initlib.qapp()))

		


################################################################################
# import initlib
Host = initlib.get_host()
Qt = initlib.get_qt_mod()
from Qt import QtCompat, QtCore, QtGui, QtWidgets
from Qt.QtCore import QPoint, QSettings, QSize, Qt
from Qt.QtWidgets import (QApplication, QCheckBox, QDialog, QDialogButtonBox,
						  QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLayout,
						  QLineEdit, QListWidget, QPushButton, QRadioButton,
						  QVBoxLayout, QWidget)


def test():
	initlib.test()

if __name__ == '__main__':
	test()