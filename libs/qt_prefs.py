try:
	from importlib import reload
except:
	pass

import json
from Qt import QtCore, QtWidgets


class QtPrefs(QtWidgets.QWidget):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def closeEvent(self, e):
		self.log.info('{} closeevent'.format(self.__class__.__name__))
		if hasattr(self, '_save'):
			self.saveUI()
		if hasattr(self, 'autoclose'):
			for i in range(self.layout.count()):
				widitem = self.layout.itemAt(i)
				if not widitem: continue
				wid = widitem.widget()
				if not wid: continue
				self.log.info('close {}'.format(wid))
				wid.close()
			self.close()
			e.accept()
		e.reject()


	def saveUI(self):
		"""Save UI state."""
		if not hasattr(self, '_save') or not hasattr(self, '_cfg') or not self._save or not self._cfg:
			return
		data = {}
		# Find UI elements
		checks = [x for x in self.findChildren(QtWidgets.QCheckBox) if x.objectName()]
		radios = [x for x in self.findChildren(QtWidgets.QRadioButton) if x.objectName()]
		sliders = [x for x in self.findChildren(QtWidgets.QSlider) if x.objectName()]
		tables = [x for x in self.findChildren(QtWidgets.QTableView) if x.objectName()]
		labels = [x for x in self.findChildren(QtWidgets.QLabel) if x.objectName()]
		lines = [x for x in self.findChildren(QtWidgets.QLineEdit) if x.objectName()]
		plaintextedits = [x for x in self.findChildren(QtWidgets.QPlainTextEdit) if x.objectName()]
		# Retrieve data
		for obj in checks:
			data[f'{self._save}.{obj.objectName()}'] = obj.isChecked()
		for obj in radios:
			data[f'{self._save}.{obj.objectName()}'] = obj.isChecked()
		for obj in sliders:
			data[f'{self._save}.{obj.objectName()}'] = obj.value()
		for obj in tables:
			model = obj.model()
			if model is None:
				continue
			val = []
			for i in range(0, model.columnCount()):
				val.append(obj.isColumnHidden(i))
			data[f'{self._save}.{obj.objectName()}'] = val
		for obj in labels:
			data[f'{self._save}.{obj.objectName()}'] = obj.text()
		for obj in lines:
			data[f'{self._save}.{obj.objectName()}'] = obj.text()
		for obj in plaintextedits:
			data[f'{self._save}.{obj.objectName()}'] = obj.toPlainText()
		jdata = json.dumps(data)
		# Save data
		self._cfg.setValue(self._save, jdata)
		self._cfg.setValue(f'{self._save}_geo', self.saveGeometry())
		self._cfg.setValue(f'{self._save}_state', int(self.state))

	def loadUI(self):
		if not hasattr(self, '_save') or not hasattr(self, '_cfg') or not self._save or not self._cfg:
			return
		geo = self._cfg.value(f'{self._save}_geo')
		if geo is not None:
			self.restoreGeometry(geo)
		state = self._cfg.value(f'{self._save}_state')
		if state is not None:
			self.state = state
		jdata = self._cfg.value(self._save)
		if not jdata:
			return
		try:
			data = json.loads(jdata)
		except Exception as e:
			self.log.exception(e)
			return
		checks = [x for x in self.findChildren(QtWidgets.QCheckBox) if x.objectName()]
		radios = [x for x in self.findChildren(QtWidgets.QRadioButton) if x.objectName()]
		sliders = [x for x in self.findChildren(QtWidgets.QSlider) if x.objectName()]
		tables = [x for x in self.findChildren(QtWidgets.QTableView) if x.objectName()]
		labels = [x for x in self.findChildren(QtWidgets.QLabel) if x.objectName()]
		lines = [x for x in self.findChildren(QtWidgets.QLineEdit) if x.objectName()]
		plaintextedits = [x for x in self.findChildren(QtWidgets.QPlainTextEdit) if x.objectName()]
		# checks
		for obj in checks:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val is not None:
				state = QtCore.Qt.Checked if val else QtCore.Qt.Unchecked
				obj.setCheckState(state)
		# radios
		for obj in radios:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val is not None:
				obj.setChecked(val)
		# sliders
		for obj in sliders:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val is not None:
				obj.setValue(val)
		# tables
		for obj in tables:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val:
				cnt = len(val)
				for i in range(0, cnt):
					obj.setColumnHidden(i, val[i])
		# labels
		for obj in labels:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# lines
		for obj in lines:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# plain text editors
		for obj in plaintextedits:
			key = f'{self._save}.{obj.objectName()}'
			val = data.get(key, None)
			if val:
				obj.document().setPlainText(val)
