import json
import logging
import sys


try:
	from .initlib import initlib
except ImportError:
	from initlib import initlib

Host = initlib.get_host()
Qt = initlib.get_qt_mod()
from Qt import QtCore, QtWidgets

__all__ = ["Prefs"]


class Prefs(QtWidgets.QWidget):
	"""QSettings mixin
	Automatically store/restore app state
	self.AUTOCLOSE	: automatically close all inner children in closeEvent()
	self.CFG		: QSettings instance, created by getCfg()
	self.SAVE		: CFG subsection name for this widget
	"""

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.AUTOCLOSE = True
		self.CFG = None
		self.SAVE = "PrefsLoader"

	def getCfg(self, app="WidgetLoader", org="joss13", domain="joss13.net"):
		if not hasattr(self, "CFG"):
			QtCore.QCoreApplication.setApplicationName(app)
			QtCore.QCoreApplication.setOrganizationName(org)
			QtCore.QCoreApplication.setOrganizationDomain(domain)
			self.CFG = QtCore.QSettings()
		return self.CFG

	@property
	def LOG(self):
		return logging.getLogger(self.__class__.__name__)

	def closeEvent(self, e):
		self.LOG.debug("closeEvent():")
		if hasattr(self, "SAVE"):
			self.saveUI(self.SAVE)
		if hasattr(self, "AUTOCLOSE"):
			layout = self.layout()
			if layout:
				for i in range(layout.count()):
					widitem = layout.itemAt(i)
					if not widitem:
						continue
					wid = widitem.widget()
					if not wid:
						continue
					self.LOG.debug(f"autoclosing {wid}")
					wid.close()
			self.close()
			e.accept()

	def saveUI(self, name, cfg=None):
		"""Save UI state.
		Args:
				cfg: QSettings object
				name: name of config section, str
		Returns: None
		"""
		cfg = cfg or self.getCfg()
		if not cfg:
			self.LOG.debug(f"saveUI(): cfg is None, skipping.")
			return
		self.LOG.debug(f"saveUI(): {cfg.fileName()}")
		data = {}
		# Find UI elements
		checks = [x for x in self.findChildren(QtWidgets.QCheckBox) if x.objectName()]
		radios = [x for x in self.findChildren(QtWidgets.QRadioButton) if x.objectName()]
		sliders = [x for x in self.findChildren(QtWidgets.QSlider) if x.objectName()]
		tables = [x for x in self.findChildren(QtWidgets.QTableView) if x.objectName()]
		labels = [x for x in self.findChildren(QtWidgets.QLabel) if x.objectName()]
		lines = [x for x in self.findChildren(QtWidgets.QLineEdit) if x.objectName()]
		plaintextedits = [x for x in self.findChildren(QtWidgets.QPlainTextEdit) if x.objectName()]
		# Retrieve data
		for obj in checks:
			data[f"{name}.{obj.objectName()}"] = obj.isChecked()
		for obj in radios:
			data[f"{name}.{obj.objectName()}"] = obj.isChecked()
		for obj in sliders:
			data[f"{name}.{obj.objectName()}"] = obj.value()
		for obj in tables:
			model = obj.model()
			if model is None:
				continue
			val = []
			for i in range(0, model.columnCount()):
				val.append(obj.isColumnHidden(i))
			data[f"{name}.{obj.objectName()}"] = val
		for obj in labels:
			data[f"{name}.{obj.objectName()}"] = obj.text()
		for obj in lines:
			data[f"{name}.{obj.objectName()}"] = obj.text()
		for obj in plaintextedits:
			data[f"{name}.{obj.objectName()}"] = obj.toPlainText()
		jdata = json.dumps(data)
		# Save data
		cfg.setValue(name, jdata)
		cfg.setValue(f"{name}_geo", self.saveGeometry())

	def loadUI(self, name, cfg=None):
		"""Restore UI state.
		Args:
				cfg: QSettings object
				name: name of config section, str
		Returns: None
		"""
		cfg = cfg or self.getCfg()
		if not cfg:
			self.LOG.debug(f"loadUI(): cfg is None, skipping.")
			return
		self.LOG.debug(f"loadUI(): {cfg.fileName()}")
		geo = cfg.value(f"{name}_geo")
		if geo is not None:
			self.restoreGeometry(geo)
		jdata = cfg.value(name)
		if not jdata:
			return
		try:
			data = json.loads(jdata)
		except Exception as e:
			self.LOG.exception(e)
			return
		checks = [x for x in self.findChildren(QtWidgets.QCheckBox) if x.objectName()]
		radios = [x for x in self.findChildren(QtWidgets.QRadioButton) if x.objectName()]
		sliders = [x for x in self.findChildren(QtWidgets.QSlider) if x.objectName()]
		tables = [x for x in self.findChildren(QtWidgets.QTableView) if x.objectName()]
		labels = [x for x in self.findChildren(QtWidgets.QLabel) if x.objectName()]
		lines = [x for x in self.findChildren(QtWidgets.QLineEdit) if x.objectName()]
		plaintextedits = [x for x in self.findChildren(QtWidgets.QPlainTextEdit) if x.objectName()]
		# checks
		for obj in checks:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val is not None:
				state = QtCore.Qt.Checked if val else QtCore.Qt.Unchecked
				obj.setCheckState(state)
		# radios
		for obj in radios:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val is not None:
				obj.setChecked(val)
		# sliders
		for obj in sliders:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val is not None:
				obj.setValue(val)
		# tables
		for obj in tables:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val:
				cnt = len(val)
				for i in range(0, cnt):
					obj.setColumnHidden(i, val[i])
		# labels
		for obj in labels:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# lines
		for obj in lines:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val is not None:
				obj.setText(val)
		# plain text editors
		for obj in plaintextedits:
			key = f"{name}.{obj.objectName()}"
			val = data.get(key, None)
			if val:
				obj.document().setPlainText(val)


class Test(Prefs, QtWidgets.QWidget):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		# Build test UI
		self.lay = QtWidgets.QVBoxLayout(self)
		self.btn = QtWidgets.QPushButton(self)
		self.lay.addWidget(self.btn)
		# Setup prefs
		self.getCfg(app=self.__class__.__name__)  # Create self.CFG if needed
		self.SAVE = f"{self.__class__.__name__}_settings"  # Define CFG subsection name
		self.loadUI(name=self.SAVE, cfg=self.CFG)


def test():
	app = None
	if initlib.get_host() == initlib.STANDALONE:
		app = QtWidgets.QApplication(sys.argv)
	root = initlib.get_main_window()
	win = Test(parent=root)
	win.show()
	if initlib.get_host() == initlib.STANDALONE:
		app.exec_()


if __name__ == "__main__":
	test()
